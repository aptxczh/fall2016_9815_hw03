# Homework 3 9815

## Author: Zhihao Chen

###How to run:
 To run the code, call the script as follows:

**python fall2016_9815_hw03.py -ac Acquisition_2015Q2.txt -pm Performance_2015Q2.txt**

This will run the code with sample inputs and print out the following:

1) Total Current UPB per Channel
2) The distribution of loans, current loans and delinquencies per State
3) The distribution of loans, current loans and delinquencies per Channel
4) The bigger seller to Fannie Mae by number of loans
5) The bigger seller to Fannie Mae by Original UPB
6) Default Rate per Channel per FICO Band.

#I discussed the work with Corbin Guan.
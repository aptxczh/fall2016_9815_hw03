from __future__ import division, print_function
import pandas as pd
import numpy as np
import argparse
def status_revert(Credit_Score):
    """
    Create the following FICO bands (credit score bands) and report the Default Rate per Channel.
    :param Credit_Score: int, borrower's credit score
    :return: string, FICO bands interval
    """
    if Credit_Score >= 780:
        return '[780+)'
    elif Credit_Score <780 and Credit_Score >= 740 :
        return '[740-780)'
    elif Credit_Score <740 and Credit_Score >= 700 :
        return '[700-740)'
    elif Credit_Score <700 and Credit_Score >= 660 :
        return '[660-700)'
    elif Credit_Score <660 and Credit_Score >= 620 :
        return '[620-660)'
    elif Credit_Score <620 and Credit_Score >= 0 :
        return '[0-620)'
    else:
        return 'Missing'
def status_revert2(delq_status):
    """
    convert delinquency to default
    :param delq_status: string, delinquency given in the data from 0 to 11
    :return: string, regrouped delinquency as default and not default
    """
    if delq_status == '0' or delq_status == '1' or delq_status == '2' or delq_status == '3' :
        return 'not default'
    else:
        return 'default'
def main(file_ac,file_pm):
    """

    :param file_ac: string, path of acquisition data file
    :param file_pm: string, path of performance data file
    :return: void, print out results
    """
    #Read needed columns from raw data
    ac_data = pd.read_csv(file_ac, sep='|', header=None, usecols=[0, 1, 2, 4, 12, 18])
    ac_data = pd.DataFrame(ac_data)
    ac_data.columns = ['Loan Identifier', 'Channel', 'Seller Name', 'Original UPB', 'Credit Score', 'Property State']

    pm_data = pd.read_csv(file_pm, sep='|', header=None, usecols=[0, 1, 4, 10])
    pm_data = pd.DataFrame(pm_data)
    pm_data.columns = ['Loan Identifier', 'Monthly Reporting Period', 'Current UPB', 'Current Loan Delinquency']
    ### Q1 ###
    #Merge two data sets on Loan Identifier
    merge_data = pd.DataFrame(pd.merge(ac_data, pm_data, on='Loan Identifier'))
    #Groupby the Loan Identifier and take the most recent entry
    merge_group = merge_data.groupby('Loan Identifier').nth(-1)
    #Aggregate UPB per Channel
    cur_UPB_per_Channel = merge_group.groupby('Channel').agg(np.sum)['Current UPB']
    print('Q1')
    print("Current UPB per Channel", cur_UPB_per_Channel)

    ### Q2 ###
    #Exclude unknown delinquency data
    merge_group_known = merge_group[merge_group['Current Loan Delinquency'] != 'x']
    #use pivot_table to get current, delinquent and total loan per state
    current_per_state = pd.pivot_table(merge_group[merge_group['Current Loan Delinquency'] == '0'],
                                       index='Property State', values='Current Loan Delinquency',
                                       fill_value=0, aggfunc=len)
    delinquent_per_state = pd.pivot_table(merge_group_known[merge_group_known['Current Loan Delinquency'] != '0'],
                                          index='Property State', values='Current Loan Delinquency',
                                          fill_value=0, aggfunc=len)
    loan_per_state = pd.pivot_table(merge_group,
                                    index='Property State', values='Current Loan Delinquency',
                                    fill_value=0, aggfunc=len)
    result_per_state = pd.DataFrame(
        {'loan': loan_per_state, 'current': current_per_state, 'delinquent': delinquent_per_state})
    print('Q2')
    print("Loan Delinquency Distribution per State")
    print(result_per_state)

    ### Q3 ###
    # use pivot_table to get current, delinquent and total loan per channel
    current_per_channel = pd.pivot_table(merge_group[merge_group['Current Loan Delinquency'] == '0'],
                                         index='Channel', values='Current Loan Delinquency',
                                         fill_value=0, aggfunc=len)
    delinquent_per_channel = pd.pivot_table(
        merge_group_known[merge_group_known['Current Loan Delinquency'] != '0'],
        index='Channel', values='Current Loan Delinquency',
        fill_value=0, aggfunc=len)
    loan_per_Channel = pd.pivot_table(merge_group,
                                      index='Channel', values='Current Loan Delinquency',
                                      fill_value=0, aggfunc=len)
    result_per_channel = pd.DataFrame(
        {'loan': loan_per_Channel, 'current': current_per_channel, 'delinquent': delinquent_per_channel})
    print('Q3')
    print("Loan Delinquency Distribution per Channel")
    print(result_per_channel)

    ### Q4 ###
    #Remove data rows with unknown or OTHER seller name
    other_removed = merge_group.dropna(subset=['Seller Name'])
    other_removed = other_removed[other_removed['Seller Name'] != 'OTHER']
    #Use pivot table to show the biggest seller by number of loans
    biggest_seller_Loan = pd.pivot_table(other_removed, index='Seller Name', values='Current Loan Delinquency',
                                         fill_value=0, aggfunc=len)
    print('Q4')
    print("The bigger seller to Fannie Mae by number of loans is {0}, with count of {1}.".
          format(biggest_seller_Loan.idxmax(), biggest_seller_Loan.max()))

    ### Q5 ###
    # Use pivot table to show the biggest seller by number of loans
    biggest_seller_UPB = pd.pivot_table(other_removed, index='Seller Name', values='Original UPB',
                                        fill_value=0, aggfunc=np.sum)
    print('Q5')
    print("The bigger seller to Fannie Mae by Original UPB is {0}, with UPB of ${1}.".
          format(biggest_seller_UPB.idxmax(), biggest_seller_UPB.max()))

    ### Q6 ###
    #Create the following FICO bands (credit score bands) and report the Default Rate per Channel.
    merge_group_known['FICO_band'] = merge_group_known['Credit Score'].apply(lambda x: status_revert(x))
    merge_group_known['Default'] = merge_group_known['Current Loan Delinquency'].apply(lambda x: status_revert2(x))
    default_table = pd.pivot_table(merge_group_known, index=['Channel', 'FICO_band'], values='Current Loan Delinquency',
                                   columns='Default', aggfunc=len, margins=True, fill_value=0)
    default_table['Default Rate%'] = default_table['default'].astype(float) / default_table['All']*100
    print('Q6')
    print(default_table['Default Rate%'])

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-ac", dest="acquisition_data_file")
    parser.add_argument("-pm",  dest="performance_data_file")
    args = parser.parse_args()
    main(file_ac=args.acquisition_data_file, file_pm=args.performance_data_file)


